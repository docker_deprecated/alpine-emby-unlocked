#!/bin/sh

#set -e

RUN_USER_NAME="${RUN_USER_NAME:-forumi0721}"

exec su-exec ${RUN_USER_NAME} /usr/bin/mono /opt/Emby.Mono/MediaBrowser.Server.Mono.exe \
	-programdata /conf.d/emby \
	-ffmpeg /usr/bin/ffmpeg \
	-ffprobe /usr/bin/ffprobe

